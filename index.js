const TelegramBot = require('node-telegram-bot-api');
const request = require('request');
const fs = require('fs');
const _ = require('lodash');
const TOKEN = '524323531:AAEzgcpFogPK3twuxMd2CbzBVcJXoi9B4hs';

const bot = new TelegramBot(TOKEN, {polling: true});

const KB = {
	currency: 'Курс валют',
	picture: 'Смотреть картинки',
	car: 'Машины',
	sport: 'Спорт',
	back: 'Назад'
};

const picSrcs = {
  [KB.car]: ['car1.jpg','car2.jpg','car3.jpg','car4.jpg','car5.jpg'],
  [KB.sport]: ['sport1.jpg','sport2.jpg','sport3.jpg','sport4.jpg','sport5.jpg']
};

bot.onText(/\/start/, message => {
  sendGreeting(message);
});

bot.on('message', message => {
	switch (message.text){
		case KB.currency:
		  sendCurrencyScreen(message.chat.id);
			break;
		case KB.picture:
			sendPictureScreen(message.chat.id);
			break;
		case KB.back:
			sendGreeting(message, false);
			break;
		case KB.car:
		case KB.sport:
		  sendPictureByName(message.chat.id, message.text);
			break;
	}
});

bot.on('callback_query', query => {

  const base = query.data;
  const symbol = 'RUB';

  bot.answerCallbackQuery({
    callback_query_id: query.id,
    text: `Вы выбрали ${base}`
  });

  request(`https://api.fixer.io/latest?symbols=${symbol}&base=${base}`, (error, response, body) => {
    if (error) throw new Error(error);
    if (response.statusCode === 200) {
      const currencyData = JSON.parse(body);

      const html = `<b>1 ${base}</b> = <em>${currencyData.rates[symbol]} ${symbol}</em>`;

      bot.sendMessage(query.message.chat.id, html, {
        parse_mode: 'HTML'
      });
    }
  });
});

function sendPictureScreen(chatId) {
	bot.sendMessage(chatId, `Что вы хотите увидеть: `, {
		reply_markup: {
			keyboard: [
				[KB.car, KB.sport],
				[KB.back]
			]
		}
	})
}

function sendGreeting(message, sayHello = true) {
	let text = sayHello
		? `Приветствую, ${message.from.first_name}\nЧто вы хотите сделать?`
		: `Что вы хотите сделать?`;
	bot.sendMessage(message.chat.id, text, {
		reply_markup: {
			keyboard: [
				[KB.currency],
				[KB.picture]
			]
		}
	});
}

function sendPictureByName (chatId, picName) {
  const srcs = picSrcs[picName];
  const src = srcs[_.random(0, srcs.length - 1)];

  bot.sendMessage(chatId, `Загружаю...`);

  fs.readFile(`${__dirname}/images/${src}`, (error, picture) => {
    if (error) throw new Error(error);
    bot.sendPhoto(chatId, picture).then(() => {
      bot.sendMessage(chatId, `Отправлено`);
    });
  })
}

function sendCurrencyScreen(chatId) {
  bot.sendMessage(chatId, `Выберете тип валюты: `, {
    reply_markup: {
      inline_keyboard: [
        [{text: 'Доллар', callback_data: 'USD'}],
        [{text: 'Евро', callback_data: 'EUR'}],
        [{text: 'Фунт', callback_data: 'GBP'}]
      ]
    }
  });
}